//
//  Tab2ViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 27/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit

class Tab2ViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource,UICollectionViewDelegate {
    var index = 1
    var catCollectionView:UICollectionView!
    var ReuseCell = "Cell"
    var Cats = [["name":"Himalayan","image":"himalayan"],["name":"Selkirk Rex","image":"selkirk_rex"],["name":"European Burmese","image":"european_burmese"]
        ,["name":"Sphynx","image":"sphynx"],["name":"Turkish Van","image":"turkish"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
initCollectionView()
       
    }
   
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Cats.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var  cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseCell, for: indexPath)
        var CatImage = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: cell.bounds.width, height: cell.bounds.height-30))
        CatImage.image = UIImage.init(named: Cats[indexPath.row]["image"]!)
        var Catname = UILabel.init(frame: CGRect.init(x: 0, y:  cell.bounds.height-30, width:cell.bounds.width , height: 30))
        CatImage.contentMode = .scaleAspectFit
        Catname.text =  Cats[indexPath.row]["name"]!
        Catname.textAlignment = .center
        cell.layer.borderWidth = 0.3
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.addSubview(CatImage)
        cell.addSubview(Catname)
        return cell
        
    }
    func initCollectionView(){
        
//        var layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
//        layout.itemSize = CGSize.init(width: self.view.bounds.width/2, height: self.view.bounds.width/2 + 200)
//        layout.footerReferenceSize=CGSize.init(width: self.view.bounds.width, height: 150)
//        
//        productsCollection = UICollectionView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 50), collectionViewLayout: layout)
//        
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
       
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
         layout.itemSize = CGSize.init(width: self.view.bounds.width/2, height: self.view.bounds.width/2 + 100 )
        
        catCollectionView = UICollectionView(frame:CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height - 110), collectionViewLayout: layout)
        catCollectionView.dataSource = self
        catCollectionView.delegate = self
        catCollectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: ReuseCell)
        catCollectionView.backgroundColor = UIColor.white
        catCollectionView.showsVerticalScrollIndicator = false
        self.view.addSubview(catCollectionView)
    }
   
}
