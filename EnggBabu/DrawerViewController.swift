//
//  DrawerViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 26/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit
protocol DrawerMethods {
    func logout()
    func AboutUs()
    func ViewMyLocation()
}
class DrawerViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    var delegate:DrawerMethods!
   var Drawerview: UIView!
    var ReuseCell  = "cell"
    var options = [" View my location","About us","Logout"]
    var optionsTableView:UITableView!
    var DismissButton:UIButton!
    override func viewWillAppear(_ animated: Bool) {
        Drawerview = UIView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width-self.view.bounds.width/3, height: self.view.bounds.height+50))
        
        self.view.addSubview(Drawerview)
        
        DismissButton = UIButton.init(frame: CGRect.init(x: self.view.bounds.width-self.view.bounds.width/3, y: 0, width: self.view.bounds.width/3, height: self.view.bounds.height
        ))
        DismissButton.backgroundColor  = UIColor.clear
        DismissButton.addTarget(self, action: #selector(goBackToHomePage), for: .touchUpInside)
        self.view.addSubview(DismissButton)
        
        
        Drawerview.alpha = 0.0
        Drawerview.center.x -= Drawerview.bounds.width+5
        
        Drawerview.layer.shadowColor = UIColor.black.cgColor
        Drawerview.layer.shadowOffset = CGSize.zero
        Drawerview.layer.shadowRadius = 5.0
        Drawerview.layer.shadowOpacity = 1.0
        Drawerview.layer.shadowPath = UIBezierPath.init(rect: Drawerview.bounds).cgPath
        
        addDrawerView()
        
        

    }
    
    func addDrawerView(){
    let image = UIImageView.init(frame: CGRect.init(x: Drawerview.bounds.width/2 - 50, y: 50, width: 100, height: 100))
    //  image.layer.borderWidth = 1
        image.layer.borderColor = UIColor.white.cgColor
        image.layer.borderWidth=1.0
        image.layer.masksToBounds = false
       
        image.layer.cornerRadius = image.frame.size.height/2
        image.clipsToBounds = true
        Drawerview.addSubview(image)
        Drawerview.bringSubview(toFront: image)
        if let url = NSURL(string: UserProfile.getUserDetails().imageUrl) {
            if let data = NSData(contentsOf: url as URL) {
                image.image = UIImage(data: data as Data)
            }
        }
       var username = UILabel.init(frame: CGRect.init(x: 5, y: 160, width: Drawerview.bounds.width-10, height: 30))
       username.textAlignment = .center
        username.textColor = UIColor.white
        username.font = UIFont.init(name: "CourierNewPS-BoldMT", size: 20)
        username.text = UserProfile.getUserDetails().userName
        username.adjustsFontForContentSizeCategory = true
       // print(UserProfile.getUserDetails().userName)
        Drawerview.addSubview(username)
        
        
        optionsTableView = UITableView.init(frame: CGRect.init(x: 5, y: 200, width:Drawerview.bounds.width-10 , height: Drawerview.bounds.height-250))
        optionsTableView.register(UITableViewCell.self, forCellReuseIdentifier: ReuseCell)
        optionsTableView.tableFooterView = UIView()
        optionsTableView.separatorColor = UIColor.clear
        optionsTableView.delegate = self
        optionsTableView.dataSource = self
        optionsTableView.backgroundColor = UIColor.black
        Drawerview.addSubview(optionsTableView)
        
    }
    
    
    func goBackToHomePage() {
        // self.Drawerview.alpha = 0.0
        UIView.animate(withDuration: 0.5, delay: 0.5, options: [], animations: {
            self.Drawerview.alpha = 0.0
            self.Drawerview.center.x -= self.Drawerview.bounds.width
        }, completion: {(finished) -> Void in
            self.dismiss(animated: true, completion: nil)}
        )
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        UIView.animate(withDuration: 0.5, delay: 0.5, options: [], animations: {
            self.Drawerview.alpha = 1.0
            self.Drawerview.center.x += self.Drawerview.bounds.width
        }, completion: nil)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

   
//MARK:Table Datasource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            self.Drawerview.center.x -= self.Drawerview.bounds.width
            self.dismiss(animated: false, completion: nil)
        delegate.ViewMyLocation()
        }
        if indexPath.row == 1 {
            self.Drawerview.center.x -= self.Drawerview.bounds.width
            self.dismiss(animated: false, completion: nil)
           // goBackToHomePage()
            delegate.AboutUs()
        }
        if indexPath.row == 2 {
            self.Drawerview.center.x -= self.Drawerview.bounds.width
            self.dismiss(animated: false, completion: nil)
            delegate.logout()
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: ReuseCell, for: indexPath)
        cell.textLabel?.text = options[indexPath.row]
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.textColor = UIColor.white
        cell.backgroundColor = UIColor.black
        cell.selectionStyle = .none
        if indexPath.row == options.count - 1 {
        cell.imageView?.image = UIImage.init(named: "logout")
        }
        return cell
    }
}
