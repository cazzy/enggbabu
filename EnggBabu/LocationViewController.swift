//
//  LocationViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 27/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit
import MapKit
class LocationViewController: UIViewController,MKMapViewDelegate{

    @IBOutlet weak var mapview: MKMapView!
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
         mapView.centerCoordinate = userLocation.location!.coordinate
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        mapview.delegate = self
         mapview.showsUserLocation = true
        
        
        let noLocation = CLLocationCoordinate2D()
        let viewRegion = MKCoordinateRegionMakeWithDistance(noLocation, 2000, 2000)
        mapview.setRegion(viewRegion, animated: false)
        
        

     
    }


    @IBAction func close(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
}
