//
//  Tab3ViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 27/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit

class Tab3ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    var index = 2
    var ReuseCell = "Cell"
    var image:UIImageView!
   var Birds = ["bird1","bird2","bird3","bird4","bird5","bird6","bird7"]
    var birdCOllectionView:UICollectionView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
image = UIImageView.init(frame: CGRect.init(x: 10, y: 100, width: self.view.bounds.width-20, height: self.view.bounds.height - 100))
        image.image = UIImage.init(named:"bird1" )
        image.contentMode = .scaleAspectFit
        self.view.addSubview(image)
       initCollectionView()
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Birds.count
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        image.image = UIImage.init(named: Birds[indexPath.row])
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = collectionView.dequeueReusableCell(withReuseIdentifier: ReuseCell, for: indexPath)
        var image = UIImageView.init(frame: CGRect.init(x: 5, y: 5, width: cell.bounds.width-10, height: cell.bounds.height-10))
        image.image = UIImage.init(named: Birds[indexPath.row])
       image.layer.shadowOffset = CGSize.init(width: 0, height: 0)
       image.layer.shadowColor = UIColor.black.cgColor
        image.layer.shadowRadius = 4
        image.layer.shadowOpacity = 0.25
        image.layer.masksToBounds = false;
        image.clipsToBounds = false;
        cell.addSubview(image)
        return cell
    }
    func initCollectionView(){
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        
        
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.itemSize = CGSize.init(width: 100, height: 100 )
        layout.scrollDirection = .horizontal
        birdCOllectionView = UICollectionView(frame:CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: 100), collectionViewLayout: layout)
        birdCOllectionView.dataSource = self
        birdCOllectionView.delegate = self
        birdCOllectionView.register(UICollectionViewCell.self, forCellWithReuseIdentifier: ReuseCell)
        birdCOllectionView.showsHorizontalScrollIndicator = false
        
        birdCOllectionView.backgroundColor = UIColor.white
        
        self.view.addSubview(birdCOllectionView)

    }

}
