//
//  HomePageViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 26/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController ,DrawerMethods{
     var pageViewController: TabsPageViewController!
    var tabs = ["Dogs","Cats","Birds"]
    var TabViews = [UIView]()
    @IBOutlet weak var tabsView: UIView!
    var SelectedTab = 0
    var x_axis:CGFloat = 0.0
    @IBOutlet weak var containerView: UIView!
    
    func createTabs(title:String,selected:Bool){
       
        
    let tab = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tabsView.bounds.width/3, height: tabsView.bounds.height))
        let tabLabel = UILabel.init(frame: CGRect.init(x: x_axis, y: 0, width: tab.bounds.width, height: tabsView.bounds.height-10))
               tabLabel.text = title
        tabLabel.textColor = UIColor.lightGray
       
        tabLabel.textAlignment = .center
        if selected {
            tabLabel.textColor = UIColor.black
            let tabbar = UIView.init(frame: CGRect.init(x: x_axis, y: tabsView.bounds.height-3, width: tab.bounds.width, height: 3))
            tabbar.backgroundColor = UIColor.black
            tab.addSubview(tabbar)
        }
        
        tab.addSubview(tabLabel)
        
        tabsView.addSubview(tab)
        x_axis =  x_axis + tab.bounds.width

        
        
    }
    func AboutUs() {
        performSegue(withIdentifier: "showAboutUs", sender: self)
        
    }
    func ViewMyLocation() {
      
        performSegue(withIdentifier: "showMap", sender: self)
    }
    func logout() {
       performSegue(withIdentifier: "backToLogin", sender: self)
       // self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
         pageViewController = self.storyboard!.instantiateViewController(withIdentifier: "tabsPagger") as! TabsPageViewController
        
       
        
      
        
       
        pageViewController.view.frame = CGRect.init(x: 0, y: 0, width: containerView.bounds.width, height: containerView.bounds.height)
        
        self.addChildViewController(pageViewController)
        self.containerView.addSubview(pageViewController.view)
        pageViewController.didMove(toParentViewController: self)
       changeTab(index: 0)
       
        // Do any additional setup after loading the view.
    }
    func changeTab(index:Int){
        x_axis = 0.0
        for view in tabsView.subviews{
        view.removeFromSuperview()
        }
        for cnt in 0..<tabs.count{
            if cnt == index {
            createTabs(title: tabs[cnt], selected: true)
            }else{
            createTabs(title: tabs[cnt], selected: false)
            }
        }
    
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDrawer"{
        var destvc = segue.destination as! DrawerViewController
            destvc.delegate = self}
    }
    @IBAction func barItemTapped(_ sender: UIBarButtonItem) {
        performSegue(withIdentifier: "showDrawer", sender: self)
    }

}
