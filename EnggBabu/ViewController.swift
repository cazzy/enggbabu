//
//  ViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 26/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn

class ViewController: UIViewController ,GIDSignInUIDelegate{
        func createLoginUI(){
        let blurEffect = UIBlurEffect(style: UIBlurEffectStyle.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(blurEffectView)
        let googleLoginButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 100, height: 100))
        googleLoginButton.setImage(UIImage.init(named: "google_login"), for: .normal)
        googleLoginButton.addTarget(self, action: #selector(showGoogleLogin), for: .touchUpInside)
       googleLoginButton.center = CGPoint.init(x: blurEffectView.center.x, y:  blurEffectView.center.y - 50)
        blurEffectView.addSubview(googleLoginButton)
        
               let fbLoginButton = UIButton.init(frame: CGRect.init(x: googleLoginButton.bounds.origin.x, y: googleLoginButton.bounds.origin.y + 100, width: 100, height: 100))
        fbLoginButton.center =  CGPoint.init(x: blurEffectView.center.x , y: blurEffectView.center.y + 50)
             fbLoginButton.setImage(UIImage.init(named: "fb_login"), for: .normal)
        fbLoginButton.addTarget(self, action: #selector(showFacebookLogin), for: .touchUpInside)
              blurEffectView.addSubview(fbLoginButton)

      
    }
    @IBAction func unwindToVC(segue:UIStoryboardSegue) { }
    
    override func viewDidAppear(_ animated: Bool) {
        LogOut()
        createLoginUI()
        
    }
    
    func LogOut(){
        let loginManager = LoginManager()
        loginManager.logOut()
         GIDSignIn.sharedInstance().signOut()
    }
    func showGoogleLogin(){
        GIDSignIn.sharedInstance().signIn()
    }
    func showFacebookLogin(){
        let loginManager = LoginManager()
        
        loginManager.logIn([ .publicProfile ], viewController: self) { loginResult in
            switch loginResult {
                
                
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                self.readProfile()
                print("Logged in!")
                
                
            }
        }
    }
    
    
    
    // Present a view that prompts the user to sign in with Google
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        // myActivityIndicator.stopAnimating()
    }

    func sign(_ signIn: GIDSignIn!,
              present viewController: UIViewController!) {
        self.present(viewController, animated: true, completion: nil)
    }
    
    // Dismiss the "Sign in with Google" view
    func sign(_ signIn: GIDSignIn!,
              dismiss viewController: UIViewController!) {
        self.dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
     
      
//        if let accessToken = AccessToken.current {
//            
//            readProfile()
//        }
       
    }
    
    
    
    
    
    struct FBProfileRequest: GraphRequestProtocol {
        typealias Response = GraphResponse
        
        var graphPath = "/me"
        var parameters: [String : Any]? = ["fields": "id, name,first_name, last_name, email, picture.type(large)"]
        var accessToken = AccessToken.current
        var httpMethod: GraphRequestHTTPMethod = .GET
        var apiVersion: GraphAPIVersion = 2.7
    }
    
   
    
    
    
    
    
    
    func presentAlertControllerFor<P: GraphRequestProtocol>(_ result: GraphRequestResult<P>) {
        let alertController: UIAlertController
        switch result {
        case .success(let response):
            alertController = UIAlertController.init(title: "Graph Request Success", message: "Graph Request Succeeded with response: \(response)", preferredStyle: .alert)
//            var data:[String:Any] = response.dictionaryValue
//            print(data["name"])
            
            
        case .failed(let error):
            alertController = UIAlertController.init(title: "Graph Request Failed", message: "Graph Request Error with response: \(error)", preferredStyle: .alert)
        }
       // present(alertController, animated: true, completion: nil)
    }
    func navigateToHomePage(){
    performSegue(withIdentifier: "showHomePage", sender: self)
    }
}



extension ViewController {
    func readProfile() {
        let request = FBProfileRequest()
        request.start { (httpResponse, result) in
            switch result {
            case .success(let response):
                print("Graph Request Succeeded: \(response)")
               
                var picture:[String:Any] = response.dictionaryValue?["picture"] as! [String : Any]
                var data:[String:Any] = picture["data"] as! [String:Any]
              
                UserProfile.StoreUserDetails(name:response.dictionaryValue?["name"] as! String , Image: data["url"] as! String )
                self.navigateToHomePage()
            case .failed(let error):
                print("Graph Request Failed: \(error)")
            }
            
            self.presentAlertControllerFor(result)
        }
    }
//    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
//        print("Did complete login via LoginButton with result \(result)")
//    }
//    
//    func loginButtonDidLogOut(_ loginButton: LoginButton) {
//        print("Did logout via LoginButton")
//    }
}

