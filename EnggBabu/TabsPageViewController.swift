//
//  TabsPageViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 27/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit

class TabsPageViewController: UIPageViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource {
    
    var index = 0
    var prev = -1
    var identifiers:[String] = ["Tab1", "Tab2","Tab3"]
    
    
    func viewControllerAtIndex(index: Int) -> UIViewController! {
        
        
        if index == 0 {
            
            return self.storyboard!.instantiateViewController(withIdentifier: "Tab1") as! Tab1ViewController
            
        }
        
        
        if index == 1 {
            
            return self.storyboard!.instantiateViewController(withIdentifier: "Tab2") as! Tab2ViewController
        }
        if index == 2 {
           
            return self.storyboard!.instantiateViewController(withIdentifier: "Tab3") as! Tab3ViewController
        }
        
        return nil
    }
    
        override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
        delegate = self
        let startingViewController = self.viewControllerAtIndex(index: self.index)
        self.setViewControllers([startingViewController!], direction: .forward, animated: true, completion: nil)
        
    }
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
         let vc = self.parent as! HomePageViewController
        if prev == self.index {
            vc.changeTab(index: 1)
        }else {
        vc.changeTab(index: self.index)
        }
        prev = self.index
    }
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let identifier = viewController.restorationIdentifier
        let index = self.identifiers.index(of: identifier!)
        
         if index == identifiers.count - 1 {
            
            return nil
        }
        
        self.index = index! + 1
       
        return self.viewControllerAtIndex(index: self.index)

    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let identifier = viewController.restorationIdentifier
        let index = self.identifiers.index(of: identifier!)
        
        if index == 0 {
            
            return nil
        }
        
        self.index = index! - 1
       
        return self.viewControllerAtIndex(index: self.index)

    }
   
}
