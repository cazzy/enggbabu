//
//  Tab1ViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 27/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit

class Tab1ViewController: UIViewController ,UITableViewDelegate,UITableViewDataSource{
    var index = 0
    var ReuseCell = "Cell"
    var Dogs = [["name":"Affenpinscher","image":"Affenpinscher"],["name":"Afghan Hound","image":"Afghan_hound"],["name":"Barbet","image":"barbet"]
    ,["name":"Bichon Frise","image":"bichon_frise"],["name":"Goldendoodle","image":"goldendoodle"]]
    
    
    var tableView:UITableView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView.init(frame: CGRect.init(x: 0, y: 0, width: self.view.bounds.width, height: self.view.bounds.height-100))
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: ReuseCell)
  //  tableView.backgroundColor = UIColor.gray
        tableView.tableFooterView = UIView()
        tableView.separatorColor = UIColor.clear
        tableView.delegate = self
        tableView.dataSource = self
        tableView.showsVerticalScrollIndicator = false
        self.view.addSubview(tableView)

       
    }
    
    //MARK:DataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Dogs.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.bounds.height/2
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ReuseCell, for: indexPath)
        
        for view in cell.subviews {
        view.removeFromSuperview()
        }
        
        cell.selectionStyle = .none
       // cell.backgroundColor = UIColor.lightGray
        let contentView = UIView.init(frame: CGRect.init(x: 5, y: 5, width: cell.bounds.width-10, height: cell.bounds.height-10))
        contentView.layer.borderWidth = 0.5
        contentView.layer.borderColor = UIColor.lightGray.cgColor
        contentView.layer.cornerRadius = 10
        contentView.clipsToBounds = true
        
        let dogImage = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: contentView.bounds.width, height: contentView.bounds.height-30))
        dogImage.image = UIImage.init(named: Dogs[indexPath.row]["image"]!)
        
        let breedName = UILabel.init(frame: CGRect.init(x: 10, y:contentView.bounds.height-30 , width: contentView.bounds.width, height: 30))
        //breedName.textAlignment = .center
        breedName.text = Dogs[indexPath.row]["name"]
        breedName.backgroundColor = UIColor.white
       // contentView.backgroundColor = UIColor.white
        contentView.addSubview(dogImage)
        contentView.addSubview(breedName)
        cell.addSubview(contentView)
        return cell
        
    }

   }
