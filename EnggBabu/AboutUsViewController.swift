//
//  AboutUsViewController.swift
//  EnggBabu
//
//  Created by SilverGlobe Solutions on 27/08/17.
//  Copyright © 2017 cazzy. All rights reserved.
//

import UIKit

class AboutUsViewController: UIViewController ,UIWebViewDelegate{
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    @IBOutlet weak var webView: UIWebView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        webView.delegate = self
webView.loadRequest(NSURLRequest(url: NSURL(string: "https://www.linkedin.com/in/bhavin-suthar-6491b3126/")! as URL) as URLRequest)
        // Do any additional setup after loading the view.
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicator.isHidden = true
        
    }
    @IBAction func close(_ sender: UIBarButtonItem) {
    
    self.dismiss(animated: true, completion: nil)
    }
   

}
